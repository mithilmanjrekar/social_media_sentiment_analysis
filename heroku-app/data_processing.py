import re


class DataPreprocessing():
    """ Whenever data is pulled in it is not ready for using it for analytics so it has to be cleaned and processed so it can be used to get sentiments out of it """

    def __init__(self, tweet_text, location, description):
        self.tweet_text = tweet_text
        self.location = location
        self.description = description

    def clean_tweet(self, content):
        """ Use sample regex statemnents to clean tweet text by removing links and special characters """
        return ' '.join(
            re.sub(
                "(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\\w+:\\/\\/\\S+)",
                " ",
                content).split())

    def de_emojify(self, content):
        """ Strip all non-ASCII characters to remove emoji characters """
        if content:
            return content.encode('ascii', 'ignore').decode('ascii')
        else:
            return ""

    def clean_de_emojify_tweet(self):
        """ Clean Tweets """
        content = self.de_emojify(self.tweet_text)
        return self.clean_tweet(content)

    def clean_de_emojify_location(self):
        """ Clean Location """
        content = self.de_emojify(self.location)
        return self.clean_tweet(content)

    def clean_de_emojify_description(self):
        """ Clean Description """
        content = self.de_emojify(self.description)
        return self.clean_tweet(content)
