import configurations
import psycopg2
import tweepy
from data_processing import DataPreprocessing
from sentiment_analysis import SentimentAnalysis


class TweetStreamer(tweepy.StreamListener):

    def on_error(self, status_code):
        """ If twitter API runs over rate limiting for developer account we stop polling in data."""

        if status_code == 420:
            # Disconnect polling tweets
            return False

    def on_status(self, status):
        """ Extract info from tweets and store it in our persistant database."""

        if status.retweeted:
            # Avoiding retweets to not repeat the content hence adding the same
            # sentiments to the existing ones.
            return True

        id_str = status.id_str
        created_at = status.created_at

        # Data Preprocessing removing unwanted data, links and special
        # characters
        data_processing = DataPreprocessing(
            status.text, status.user.location, status.user.description)
        text = data_processing.clean_de_emojify_tweet()
        user_location = data_processing.clean_de_emojify_location()
        user_description = data_processing.clean_de_emojify_description()

        # Sentimental analysis of the tweet text
        tweet_sentiment = SentimentAnalysis(text)
        polarity = tweet_sentiment.get_polarity()
        subjectivity = tweet_sentiment.get_subjectivity()

        user_created_at = status.user.created_at
        user_followers_count = status.user.followers_count
        longitude, latitude = None, None
        if status.coordinates:
            longitude = status.coordinates['coordinates'][0]
            latitude = status.coordinates['coordinates'][1]

        retweet_count = status.retweet_count
        favorite_count = status.favorite_count
        print("\n Tweet Json:-", status)
        # Store all data in Heroku PostgreSQL
        cur = conn.cursor()
        select_query = "INSERT INTO {} (id_str, created_at, text, polarity, subjectivity, user_created_at, user_location, user_description, user_followers_count, longitude, latitude, retweet_count, favorite_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)".format("tweets")
        values = (
            id_str,
            created_at,
            text,
            polarity,
            subjectivity,
            user_created_at,
            user_location,
            user_description,
            user_followers_count,
            longitude,
            latitude,
            retweet_count,
            favorite_count)
        cur.execute(select_query, values)
        conn.commit()
        delete_query = '''
        DELETE FROM {0}
        WHERE id_str IN (
            SELECT id_str
            FROM {0}
            ORDER BY created_at asc
            LIMIT 200) AND (SELECT COUNT(*) FROM tweets) > 9600;
        '''.format("tweets")
        cur.execute(delete_query)
        conn.commit()
        cur.close()


DATABASE_URL = configurations.DATABASE_URL
conn = psycopg2.connect(DATABASE_URL, sslmode='require')
cur = conn.cursor()
""" Check if this table exits. If not, then create a new one. """
cur.execute("""
        SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = '{0}'
        """.format("tweets"))
if cur.fetchone()[0] == 0:
    cur.execute(
        "CREATE TABLE {} ({});".format(
            "tweets", "id_str VARCHAR(255), created_at timestamp DEFAULT NULL, text VARCHAR(255), \
            polarity INT, subjectivity INT, user_created_at VARCHAR(255), user_location VARCHAR(255), \
            user_description VARCHAR(255), user_followers_count INT, longitude DOUBLE PRECISION , latitude DOUBLE PRECISION , \
            retweet_count INT, favorite_count INT"))
    cur.execute(
        "CREATE TABLE {} ({});".format(
            "analytics",
            "total_daily_users DOUBLE PRECISION, total_daily_tweets DOUBLE PRECISION, total_daily_impressions DOUBLE PRECISION"))
    sql = "INSERT INTO {} (total_daily_users, total_daily_tweets, total_daily_impressions) VALUES (%s, %s, %s)".format(
        "analytics")
    cur.execute(sql, (0, 0, 0))
    conn.commit()
cur.close()

auth = tweepy.OAuthHandler(
    configurations.CREDENTIALS["API_KEY"],
    configurations.CREDENTIALS["API_SECRET"])
auth.set_access_token(
    configurations.CREDENTIALS["ACCESS_TOKEN"],
    configurations.CREDENTIALS["ACCESS_SECRET"])
api = tweepy.API(auth)
myStreamListener = TweetStreamer()
myStream = tweepy.Stream(auth=api.auth, listener=myStreamListener)
myStream.filter(languages=["en"], track=configurations.KEYWORDS)
conn.close()
