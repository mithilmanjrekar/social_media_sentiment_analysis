import dash
import psycopg2
import datetime
import configurations
import pandas as pd
import plotly.graph_objs as go
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

# Reference https://dash.plotly.com/layout
app = dash.Dash(__name__, external_stylesheets=[
                'https://codepen.io/chriddyp/pen/bWLwgP.css'])
app.title = 'Social Media Sentiment Analysis'
server = app.server
app.layout = html.Div(children=[
    html.H1('Social Media Sentiment Analysis ', style={
        'textAlign': 'center', 'padding-bottom': '20px'
    }),
    html.H5('LIVE TRACKING ...', style={
        'textAlign': 'center', 'padding-bottom': '10px', "color": 'red'
    }),
    html.Div(id='live-update-graph'),
    dcc.Interval(
        id='interval-component-slow',
        interval=1 * 4000,  # in milliseconds
        n_intervals=0
    )
], style={'width': '100%', 'padding-top': '70px', 'padding-bottom': '70px'})


# Refernce https://dash.plotly.com/basic-callbacks
@app.callback(Output('live-update-graph', 'children'),
              [Input('interval-component-slow', 'n_intervals')])
def update_graph_live(n):

    # Pointing to the heroku database and polling the updated tweets to keep
    # the fronend and backend de coupled from each other.
    DATABASE_URL = configurations.DATABASE_URL
    conn = psycopg2.connect(DATABASE_URL, sslmode='require')
    query = "SELECT id_str, text, created_at, polarity, user_location, user_followers_count FROM {}".format(
        "tweets")
    tweets_data_frame = pd.read_sql(query, con=conn)
    print(
        f"\n Rendering analytics for {len(tweets_data_frame.index)} tweets to React App.........")
    # Convert UTC into UK current day loight savings timings
    tweets_data_frame['created_at'] = pd.to_datetime(
        tweets_data_frame['created_at']).apply(
        lambda x: x +
        datetime.timedelta(
            hours=1))

    # Creating time series by cleaning and transforming the data frames and
    # gettting analytics to be plot on the graph
    outcome = tweets_data_frame.groupby(
        [
            pd.Grouper(
                key='created_at',
                freq='10s'),
            'polarity']).count().unstack(
                fill_value=0).stack().reset_index()
    outcome = outcome.rename(
        columns={
            "id_str": "Num of '{}' mentions".format(
                configurations.KEYWORDS[0]),
            "created_at": "Time"})
    plot_time_line = outcome["Time"][outcome['polarity']
                                     == 0].reset_index(drop=True)
    query = "SELECT total_daily_users, total_daily_tweets, total_daily_impressions FROM analytics;"
    analytics = pd.read_sql(query, con=conn)
    total_daily_tweets = analytics['total_daily_tweets'].iloc[0] + outcome[-6:- \
        3]["Num of '{}' mentions".format(configurations.KEYWORDS[0])].sum()
    total_daily_impressions = analytics['total_daily_impressions'].iloc[0] + tweets_data_frame[tweets_data_frame['created_at'] > (
        datetime.datetime.now() + datetime.timedelta(hours=1, minutes=0))]['user_followers_count'].sum()
    cur = conn.cursor()
    uk_time = datetime.datetime.now() + datetime.timedelta(hours=1, minutes=0)
    if uk_time.strftime("%H%M") == '0000':
        cur.execute(
            "UPDATE analytics SET total_daily_tweets = 0, total_daily_impressions = 0;")
    else:
        cur.execute(
            "UPDATE analytics SET total_daily_tweets = {}, total_daily_impressions = {};".format(
                total_daily_tweets,
                total_daily_impressions))
    conn.commit()
    cur.close()
    conn.close()

    # Create the graph
    children = [
        html.Div([
            html.Div([
                dcc.Graph(
                    id='crossfilter-indicator-scatter',
                    figure={
                        'data': [
                            go.Scatter(
                                x=plot_time_line,
                                y=outcome["Num of '{}' mentions".format(
                                    configurations.KEYWORDS[0])][outcome['polarity'] == 0].reset_index(drop=True),
                                name="Neutrals",
                                opacity=0.8,
                                mode='lines',
                                line=dict(width=0.5, color='rgb(0, 76, 153)'),
                                stackgroup='one'
                            ),
                            go.Scatter(
                                x=plot_time_line,
                                y=outcome["Num of '{}' mentions".format(
                                    configurations.KEYWORDS[0])][outcome['polarity'] == -1].reset_index(drop=True).apply(lambda x: -x),
                                name="Negatives",
                                opacity=0.8,
                                mode='lines',
                                line=dict(width=0.5, color='rgb(153, 0, 0)'),
                                stackgroup='two'
                            ),
                            go.Scatter(
                                x=plot_time_line,
                                y=outcome["Num of '{}' mentions".format(
                                    configurations.KEYWORDS[0])][outcome['polarity'] == 1].reset_index(drop=True),
                                name="Positives",
                                opacity=0.8,
                                mode='lines',
                                line=dict(width=0.5, color='rgb(0, 153, 0)'),
                                stackgroup='three'
                            )
                        ]
                    }
                )
            ], style={'width': '100%', 'display': 'inline-block', 'padding': '20 20 20 20', 'background': "#E0E0E0"})
        ]),

        html.Div(
            className='row',
            children=[
                html.Div(
                    children=[
                        html.P(f"Currently tracking tweets for keyword '{configurations.KEYWORDS[0]}' on Twitter.",
                               style={
                                   'fontSize': 25
                               }
                               ),
                    ],
                    style={
                        'width': '55%',
                        'display': 'inline-block'
                    }
                ),
                html.Div(
                    children=[
                        html.P(f'Total Tweets Posted on {uk_time.strftime("%B")} {uk_time.strftime("%d")}, {uk_time.strftime("%Y")}',
                               style={
                                   'fontSize': 20
                               }
                               ),
                        html.P('{}'.format(total_daily_tweets),
                               style={
                            'fontSize': 40
                        }
                        )
                    ],
                    style={
                        'width': '30%',
                        'display': 'inline-block'
                    }
                ),

            ],
            style={'marginLeft': 70}
        )
    ]
    return children


if __name__ == '__main__':
    app.run_server(debug=False)
