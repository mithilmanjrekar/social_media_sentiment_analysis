from textblob import TextBlob


class SentimentAnalysis():
    """ For now a library called TextBlob has been used for NLP and can be replced with advanced libraries and self made algorithms as a future scope """

    def __init__(self, tweet_text):
        self.tweet_text = tweet_text
        self.sentiment = TextBlob(self.tweet_text).sentiment

    def get_polarity(self):
        """ Get the polarity for the text usually returned as -1(negative),0(neutral),1(positive)  after analysing the text using NLP """
        return self.sentiment.polarity

    def get_subjectivity(self):
        """ Get the subjectivity for the text """
        return self.sentiment.subjectivity
