## Group 6 Project - Univeristy of Liverpool 

### Project Requirements:-
Python 3.9 Installation & Setup Guide.
https://realpython.com/installing-python/

### Packages that need to be installed:-
If pip is not installed use the link to install it. 
https://pip.pypa.io/en/stable/installing/

#### When pip is installed install the following package:-
`<pip install -r requirement.txt>`

### Running the code:-
1. Python file is in the main project folder called heroku-app
2. Run the Backend and the Frontend with the following commands to test in on local or development environment.
To run use following commands:-
`<python heroku-app/backend_poll_tweets.py>`
`<python heroku-app/frontend_display_tweets.py>`

### Prequisites
1. Register account for Twitter developer portal https://developer.twitter.com/en/docs/getting-started
2. Create a Heroku account for deployment https://signup.heroku.com/
3. Create Postgres database on Heroku https://devcenter.heroku.com/articles/heroku-postgresql
4. Version control understanding in out case BitBucket https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud
5. Understand deployments on Heroku with siomple Git commands https://devcenter.heroku.com/categories/python-support


### Code Summary
<p align="center">
![DFD](data_flow_diagram.png)
</p>
The code contains Backend and Frontend files to be run in Python.
We have used frameworks around python for both since we would have taken lot of learning for creating custom Frontend. Fortunately we have libraries that help us code using basic Knowledge from HTML, CSS and JavaScript.
Backend is reponsible for polling tweets from twitter API and cleaning them for pushing them into the Heroku Postgres database. 
Frontend is responsible for polling tweets from the Heroku Postgres database and displayiong them on the Frontend Visual dashboard.


### Backend
<p align="center">
		![Backend](backend.gif)
</p>

### Frontend
Insert http://127.0.0.1:8050/ into server for running on local.

<p align="center">
		![Frontebnd](frontend.gif)
</p>

### Hosting
<p align="center">
![Heroku](heroku.png)
</p>



